package com.verianggoro.tmdbmovie.common

object Config {
    const val Main_Url = "https://api.themoviedb.org/"
    const val GENRES_PATH = "3/genre/movie/list"
    const val DISCOVER_PATH = "3/discover/movie"
    const val DETAIL_PATH = "3/movie"
    const val REVIEW_USER = "reviews"
    const val VIDEO_TRAILER = "videos"

    const val TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3Y2RkMzI2NTYwZDlhZGI1ZTI0YWUzYWRkMWM3ZDMzNSIsInN1YiI6IjVhN2U3MTc2OTI1MTQxNDBmODAwM2M2ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.RiITCzswmTCBr9Ro5YkQZ_FfEvXdzkMunvka_a9woHg"

    const val BASE_URL_ASSETS = "https://image.tmdb.org/t/p/w500"

}